// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "EnemiesControlSubsystem.generated.h"

class AEnemySpawnerBase;
class AEnemyCharacterBase;
/**
 * 
 */
UCLASS()
class PROJECTZERO_API UEnemiesControlSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	
public:
	void RegisterAnEnemy(TWeakObjectPtr<AEnemyCharacterBase> EnemyCharacterBase);
	void RegisterASpawner(TWeakObjectPtr<AEnemySpawnerBase> EnemySpawnerBase);
	
protected:
	UPROPERTY()
	TArray<TWeakObjectPtr<AEnemyCharacterBase>> AllEnemies{};

	UPROPERTY()
	TArray<TWeakObjectPtr<AEnemySpawnerBase>> AllSpawners{};	
};

