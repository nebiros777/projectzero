// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectZero/Enemies/EnemySpawnerBase.h"

#include "EnemiesControlSubsystem.h"
#include "EnemyCharacterBase.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Components/BoxComponent.h"

// Sets default values
AEnemySpawnerBase::AEnemySpawnerBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>("BoxComponent");
}

// Called when the game starts or when spawned
void AEnemySpawnerBase::BeginPlay()
{
	Super::BeginPlay();

	if(auto EnemiesControlSubsystem = GetWorld()->GetGameInstance()->GetSubsystem<UEnemiesControlSubsystem>())
	{
		EnemiesControlSubsystem->RegisterASpawner(this);
	}
}

// Called every frame
void AEnemySpawnerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnemySpawnerBase::SpawnEnemy()
{
	if (EnemyClass.IsPending())
	{
		EnemyClass.LoadSynchronous();
	}
	
	APawn* SpawnedPawn = UAIBlueprintHelperLibrary::SpawnAIFromClass(GetWorld(), EnemyClass.Get(), nullptr
		, BoxComponent->GetComponentLocation()
		, BoxComponent->GetComponentRotation()
		, /*bNoCollisionFail=*/true);
}

