// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectZero/Enemies/EnemiesControlSubsystem.h"

#include "EnemySpawnerBase.h"


void UEnemiesControlSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	FTimerHandle SpawnHandler;
	GetWorld()->GetTimerManager().SetTimer(SpawnHandler, FTimerDelegate::CreateWeakLambda(this, [this, &SpawnHandler]()
	{
		if (AllEnemies.Num() < 50)
		{
			for (auto& Spawner : AllSpawners)
			{
				Spawner->SpawnEnemy();
			}
		}
		else
		{
			GetWorld()->GetTimerManager().ClearTimer(SpawnHandler);
		}
	}), 0.5f, true);
}

void UEnemiesControlSubsystem::RegisterAnEnemy(TWeakObjectPtr<AEnemyCharacterBase> EnemyCharacterBase)
{
	AllEnemies.AddUnique(EnemyCharacterBase);
}

void UEnemiesControlSubsystem::RegisterASpawner(TWeakObjectPtr<AEnemySpawnerBase> EnemySpawnerBase)
{
	AllSpawners.AddUnique(EnemySpawnerBase);
}
