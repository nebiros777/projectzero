// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawnerBase.generated.h"

class UBoxComponent;
class AEnemyCharacterBase;
UCLASS()
class PROJECTZERO_API AEnemySpawnerBase : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	UBoxComponent* BoxComponent;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<AEnemyCharacterBase> EnemyClass;

public:	
	// Sets default values for this actor's properties
	AEnemySpawnerBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SpawnEnemy();
};
