// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectZero/Enemies/EnemyCharacterBase.h"
#include "EnemiesControlSubsystem.h"

// Sets default values
AEnemyCharacterBase::AEnemyCharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
	StaticMeshComponent->SetupAttachment(GetRootComponent());
}

// Called when the game starts or when spawned
void AEnemyCharacterBase::BeginPlay()
{
	Super::BeginPlay();

	if(auto EnemiesControlSubsystem = GetWorld()->GetGameInstance()->GetSubsystem<UEnemiesControlSubsystem>())
	{
		EnemiesControlSubsystem->RegisterAnEnemy(this);
	}
	
}

// Called every frame
void AEnemyCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEnemyCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyCharacterBase::ReceiveHitFromProjectile()
{
	this->Destroy(true,true);
}

