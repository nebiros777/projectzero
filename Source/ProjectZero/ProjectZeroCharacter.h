// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ProjectZeroCharacter.generated.h"

class AWeaponBase;
UCLASS(Blueprintable)
class AProjectZeroCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AProjectZeroCharacter();

	virtual void BeginPlay() override;
	
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:
	void SetToMouseCursorRotation(FRotator NewRotation);

private:
	void SpawnWeapon();
	void StartShooting();
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWeaponBase> WeaponClass;
	
protected:
	UPROPERTY()
	TSoftObjectPtr<AWeaponBase> CurrentWeapon;

	FTimerHandle ShootCooldownTimer;

	FRotator ToMouseCursorRotation;
};

