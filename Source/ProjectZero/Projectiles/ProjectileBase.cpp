// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectZero/Projectiles/ProjectileBase.h"

#include "ProjectZero/Enemies/EnemyCharacterBase.h"

void AProjectileBase::OnEnemyOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                     UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (auto Enemy = Cast<AEnemyCharacterBase>(OtherActor))
	{
		Enemy->ReceiveHitFromProjectile();
		
		GetWorldTimerManager().ClearTimer(DestroyTimer);
		
		this->Destroy(true,true);
	}
}

// Sets default values
AProjectileBase::AProjectileBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	EnemyOverlapComponent = CreateDefaultSubobject<USphereComponent>("EnemyOverlapComponent");
	EnemyOverlapComponent->OnComponentBeginOverlap.AddUniqueDynamic(this, &ThisClass::OnEnemyOverlap);
	SetRootComponent(EnemyOverlapComponent);
	
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovementComponent");
	ProjectileMovementComponent->InitialSpeed = ProjectileSettings.InitialSpeed;
	ProjectileMovementComponent->MaxSpeed = ProjectileSettings.InitialSpeed;
		
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
	StaticMeshComponent->SetupAttachment(GetRootComponent());
}

// Called when the game starts or when spawned
void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(DestroyTimer, FTimerDelegate::CreateWeakLambda(this, [this]()
	{
		this->Destroy(true,true);
	}), 10, false);
	
}

// Called every frame
void AProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AProjectileBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

