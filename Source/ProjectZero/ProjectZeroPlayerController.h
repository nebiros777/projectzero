// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ProjectZeroPlayerController.generated.h"

class AProjectZeroCharacter;
/** Forward declaration to improve compiling times */
class UNiagaraSystem;

UCLASS()
class AProjectZeroPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AProjectZeroPlayerController();

	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputAction* MoveLeftAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* MoveRightAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* MoveUpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* MoveDownAction;
	
protected:

	virtual void SetupInputComponent() override;


	virtual void Tick(float DeltaSeconds) override;
	// To add mapping context
	virtual void BeginPlay();

	virtual void OnPossess(APawn* InPawn) override;

	/** Input handlers for SetDestination action. */
	void OnMoveLeftStarted();	
	void OnMoveRightStarted();
	void OnMoveUpStarted();
	void OnMoveDownStarted();
	
	void SetControlledPawnShootRotation();
		
private:
	FVector CachedDestination;

	TWeakObjectPtr<AProjectZeroCharacter> CharacterRef{nullptr};
};


