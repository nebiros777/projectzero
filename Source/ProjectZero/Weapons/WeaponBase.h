// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponBase.generated.h"

class AProjectileBase;

USTRUCT(BlueprintType)
struct FWeaponSettings
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	float ShootCooldownDuration{1.2f};

	UPROPERTY(EditDefaultsOnly)
	int32 AmountOfBullets{3};

	UPROPERTY(EditDefaultsOnly)
	float BurstTemp{0.2f};
	
	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<AProjectileBase> ProjectileClass;
};

UCLASS()
class PROJECTZERO_API AWeaponBase : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* StaticMeshComponent;
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FWeaponSettings GetWeaponSettings() const;
	
private:
	UPROPERTY(EditDefaultsOnly)
	FWeaponSettings WeaponSettings;
	
};