// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProjectZeroCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Projectiles/ProjectileBase.h"
#include "Weapons/WeaponBase.h"

AProjectZeroCharacter::AProjectZeroCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Destructible, ECR_Ignore);
	
	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 1200.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AProjectZeroCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	FTimerHandle StartTimer;
	GetWorldTimerManager().SetTimer(StartTimer, FTimerDelegate::CreateWeakLambda(this, [this]()
	{
		SpawnWeapon();
		StartShooting();
	}), 1.0f, false);
}

void AProjectZeroCharacter::SetToMouseCursorRotation(FRotator NewRotation)
{
	ToMouseCursorRotation = NewRotation;
}

void AProjectZeroCharacter::SpawnWeapon()
{
	if (!IsValid(WeaponClass))
	{
		return;
	}
	
	FTransform WeaponTransform;
	FVector Location = GetCapsuleComponent()->GetComponentLocation();
	Location.Z += 50.f;
	WeaponTransform.SetLocation(Location);
	WeaponTransform.SetRotation(FQuat(GetCapsuleComponent()->GetComponentRotation()));

	AActor* SpawnedWeapon = UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), WeaponClass,
		WeaponTransform);
	
	UGameplayStatics::FinishSpawningActor(SpawnedWeapon,WeaponTransform);
	if (Cast<AWeaponBase>(SpawnedWeapon))
	{
		CurrentWeapon = Cast<AWeaponBase>(SpawnedWeapon);
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepWorldTransform);
	}
}

void AProjectZeroCharacter::StartShooting()
{
	if (!CurrentWeapon.IsValid())
	{
		return;
	}
	FTimerDelegate CooldownDelegate = FTimerDelegate::CreateWeakLambda(this, [this]()
	{				
		for (int i = 1; i < CurrentWeapon->GetWeaponSettings().AmountOfBullets + 1; i++)
		{
			FTimerHandle BurstTimer;
			FTimerDelegate BurstDelegate = FTimerDelegate::CreateWeakLambda(this, [this]()
			{
				if (CurrentWeapon->GetWeaponSettings().ProjectileClass.IsPending())
				{
					CurrentWeapon->GetWeaponSettings().ProjectileClass.LoadSynchronous();
				}
	
				FTransform ProjectileTransform;
				ProjectileTransform.SetLocation(CurrentWeapon->GetActorLocation());
				
				ProjectileTransform.SetRotation(FQuat(ToMouseCursorRotation));

				AActor* SpawnedBullet = UGameplayStatics::BeginDeferredActorSpawnFromClass(GetWorld(), CurrentWeapon->GetWeaponSettings().ProjectileClass.Get(),
					ProjectileTransform);	
				UGameplayStatics::FinishSpawningActor(SpawnedBullet,ProjectileTransform);
	
			});
			GetWorldTimerManager().SetTimer(BurstTimer, BurstDelegate, CurrentWeapon->GetWeaponSettings().BurstTemp * i, false);
		}
	});
	GetWorldTimerManager().SetTimer(ShootCooldownTimer, CooldownDelegate, CurrentWeapon->GetWeaponSettings().ShootCooldownDuration, true);
}

void AProjectZeroCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}
