// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProjectZeroPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "ProjectZeroCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

AProjectZeroPlayerController::AProjectZeroPlayerController()
{
	PrimaryActorTick.bCanEverTick = true;
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::CardinalCross;
	CachedDestination = FVector::ZeroVector;	
}

void AProjectZeroPlayerController::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}
}

void AProjectZeroPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	CharacterRef = Cast<AProjectZeroCharacter>(InPawn);
}

void AProjectZeroPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent))
	{
		EnhancedInputComponent->BindAction(MoveLeftAction, ETriggerEvent::Started, this, &AProjectZeroPlayerController::OnMoveLeftStarted);
		EnhancedInputComponent->BindAction(MoveLeftAction, ETriggerEvent::Triggered, this, &AProjectZeroPlayerController::OnMoveLeftStarted);
		
		EnhancedInputComponent->BindAction(MoveRightAction, ETriggerEvent::Started, this, &AProjectZeroPlayerController::OnMoveRightStarted);
		EnhancedInputComponent->BindAction(MoveRightAction, ETriggerEvent::Triggered, this, &AProjectZeroPlayerController::OnMoveRightStarted);
		
		EnhancedInputComponent->BindAction(MoveUpAction, ETriggerEvent::Started, this, &AProjectZeroPlayerController::OnMoveUpStarted);
		EnhancedInputComponent->BindAction(MoveUpAction, ETriggerEvent::Triggered, this, &AProjectZeroPlayerController::OnMoveUpStarted);
		
		EnhancedInputComponent->BindAction(MoveDownAction, ETriggerEvent::Started, this, &AProjectZeroPlayerController::OnMoveDownStarted);
		EnhancedInputComponent->BindAction(MoveDownAction, ETriggerEvent::Triggered, this, &AProjectZeroPlayerController::OnMoveDownStarted);
	}
}

void AProjectZeroPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CharacterRef.IsValid())
	{
		SetControlledPawnShootRotation();
	}
}

void AProjectZeroPlayerController::SetControlledPawnShootRotation()
{
	FHitResult Hit;
	bool bHitSuccessful = false;
	bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);
	

	// If we hit a surface, cache the location
	if (bHitSuccessful)
	{
		CachedDestination = Hit.Location;
	}

	FVector WorldDirection = (CachedDestination - CharacterRef->GetActorLocation()).GetSafeNormal();
	WorldDirection.Z = 0.0f;
	
	CharacterRef->SetToMouseCursorRotation(WorldDirection.Rotation());
	
	/*
	// If it was a short press
	if (FollowTime <= ShortPressThreshold)
	{
		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}

	FollowTime = 0.f;
	*/
}

void AProjectZeroPlayerController::OnMoveLeftStarted()
{
	if (APawn* ControlledPawn = GetPawn())
	{
		ControlledPawn->AddMovementInput(FVector(0,1,0), -1.0, false);
	}
}

void AProjectZeroPlayerController::OnMoveRightStarted()
{
	if (APawn* ControlledPawn = GetPawn())
	{
		ControlledPawn->AddMovementInput(FVector(0,1,0), 1.0, false);
	}
}

void AProjectZeroPlayerController::OnMoveUpStarted()
{
	if (APawn* ControlledPawn = GetPawn())
	{
		ControlledPawn->AddMovementInput(FVector(1,0,0), 1.0, false);
	}
}

void AProjectZeroPlayerController::OnMoveDownStarted()
{
	if (APawn* ControlledPawn = GetPawn())
	{
		ControlledPawn->AddMovementInput(FVector(1,0,0), -1.0, false);
	}
}
